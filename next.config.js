require("dotenv").config();
const webpack = require("webpack");
const { parsed: myEnv } = require("dotenv").config({
  path: ".env",
});

module.exports = async () => {
  /**
   * @type {import('next').NextConfig}
   *
   *
   */

  const nextConfig = {
    webpack(config) {
      config.plugins.push(new webpack.EnvironmentPlugin(myEnv));
      return config;
    },
    reactStrictMode: true,
    images: {
      minimumCacheTTL: 60,
      domains: [
        "www.cuponstar.com"
      ],
    },
  };
  return nextConfig;
};
