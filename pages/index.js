import Head from 'next/head'
import Panel from '../components/interface/Panel'
import TasksList from '../components/TasksList'

export default function Home() {
  return (
    <>
      <Head>
        <title>Todo App</title>
        <meta name="description" content="Aplicación para prueba técnica Cuponstar" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className='w-full'>
        <TasksList/>
      </main>

    </>
  )
}
