import Panel from "../components/interface/Panel";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <div className="flex flex-col lg:flex-row">
      <Panel />
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
