import { useState, useCallback, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import { useRouter } from "next/router";
import CircularProgress from "@mui/material/CircularProgress";

const Form = () => {
  const router = useRouter();

  const [formData, setFormData] = useState({
    name: "",
    description: "",
  });
  const [errors, setErrors] = useState({});
  const [disabled, setDisabled] = useState(true);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (router.query.task_id) {
      try {
        axios
          .get(`${process.env.API_BASE_URL}api/tasks/${router.query.task_id}`)
          .then((resp) => {
            const task = resp.data.data;
            setFormData({
              ...formData,
              name: task.name,
              description: task.description,
            });
          })
          .catch((e) => console.log(e));
      } catch (error) {
        console.log(error);
      }
    }
  }, [router]);

  useEffect(() => {
    if (formData.name.length && formData.description.length) {
      setDisabled(false);
    } else {
      if (!disabled) {
        setDisabled(true);
      }
    }
  }, [formData.name, formData.description]);

  const validate = useCallback((formData) => {
    const { name, description } = formData;

    const errors = {};
    if (!name || name.length > 1000 || !name.length)
      errors.message = "Debe colocar un mensaje";
    if (!description || description.length > 1000 || !description.length)
      errors.message = "Debe colocar un mensaje";

    setErrors(errors);
  }, []);

  const submitForm = (e) => {
    e.preventDefault();
    setDisabled(true);
    setLoading(true);

    if (router.query.task_id) {
      try {
        axios
          .put(`${process.env.API_BASE_URL}api/tasks/${router.query.task_id}`, formData)
          .then((resp) => {
            Swal.fire({
              position: "bottom-end",
              icon: "success",
              title: resp.data.message,
              showConfirmButton: false,
              timer: 1500,
            });

            setTimeout(() => {
              router.push("/");
            }, 2000);
          })
          .catch((e) => console.log(e));
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        axios
          .post(`${process.env.API_BASE_URL}api/tasks`, formData)
          .then((resp) => {
            Swal.fire({
              position: "bottom-end",
              icon: "success",
              title: resp.data.message,
              showConfirmButton: false,
              timer: 1500,
            });

            setTimeout(() => {
              router.push("/");
            }, 2000);
          })
          .catch((e) => console.log(e));
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <div className="mt-8 w-full ml-4 flex flex-col gap-4">
      <div>
        <h1 className="text-semibold text-2xl">Crear nueva tarea</h1>
      </div>
      <form onSubmit={submitForm} className="mt-2 flex flex-col gap-4">
        <input
          autoComplete="off"
          maxLength={50}
          type="text"
          name="name"
          value={formData.name}
          onChange={(e) => setFormData({ ...formData, name: e.target.value })}
          id="name"
          placeholder="Nombre *"
          className={`h-[51px] bg-[#F1F1F1] rounded-md py-2 w-[95%] lg:w-2/3 text-left pl-4 placeholder:text-gray-500 transition-all duration-200 ${
            errors?.name ? "border-2 border-red-400" : ""
          }`}
        />
        <textarea
          value={formData.description}
          onChange={(e) =>
            setFormData({ ...formData, description: e.target.value })
          }
          placeholder="Descripción *"
          className={`bg-[#f1f1f1] w-[95%] lg:w-2/3 rounded-lg p-4  ${
            errors?.message ? "border-2 border-red-600" : ""
          }`}
          name="description"
          id="description"
          cols="30"
          rows="7"
          maxLength={1000}
        />
        <button
          disabled={disabled}
          type="submit"
          className={`${
            disabled ? "bg-[#a46ff0]" : "bg-[#783fca]"
          } hover:bg-[#a46ff0] w-[95%] lg:w-1/6 p-2 rounded-xl text-white transition-all duration-300 flex items-center justify-center`}
        >
          {loading ? (
            <CircularProgress className="loading" />
          ) : (
            `${router.query.task_id ? "Actualizar" : "Crear"}`
          )}
        </button>
      </form>
    </div>
  );
};

export default Form;
