import Image from "next/image";
import React from "react";
import Button from "./Button";

const Panel = () => {
  return (
    <section className="w-full lg:w-1/6 lg:border-2 lg:border-r-gray-200 flex flex-col gap-4 p-2 h-[20%] lg:h-[100vh]">
      <Image
        className="mt-6 mb-2 mx-auto"
        height={26}
        width={168}
        src="https://www.cuponstar.com/hubfs/logo%20(2).svg"
        alt="logo-cuponstar"
      />
      <div className="flex flex-col gap-4">
        <Button funct={"home"} />
        <Button funct={"create"} />
      </div>
    </section>
  );
};

export default Panel;
