import Link from "next/link";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import { useRouter } from "next/router";
import CircularProgress from "@mui/material/CircularProgress";

const Button = ({ funct, task_id }) => {
  const router = useRouter();

  const [text, setText] = useState("");
  const [redirect, setRedirect] = useState("");
  const [loading, setLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    if (funct === "home") {
      setText("HOME");
      setRedirect("/");
    }

    if (funct === "create") {
      setText("CREAR TAREA");
      setRedirect("/tarea/form");
    }

    if (funct === "edit") {
      setText("EDITAR");
      setRedirect(`/tarea/form?task_id=${task_id}`);
    }

    if (funct === "delete") {
      setText("ELIMINAR");
    }
  }, [funct]);

  const deleteTask = () => {
    setDisabled(true);
    setLoading(true);
    try {
      axios
        .delete(`${process.env.API_BASE_URL}api/tasks/${task_id}`)
        .then((resp) => {
          Swal.fire({
            position: "bottom-end",
            icon: "success",
            title: resp.data.message,
            showConfirmButton: false,
            timer: 1500,
          });

          setTimeout(() => {
            location.reload()
          }, 2000);
        })
        .catch((e) => console.log(e));
    } catch (error) {
      console.log(error);
    }
  };

  return funct === "delete" ? (
    <button
      disabled={disabled}
      onClick={deleteTask}
      className="w-[27%] lg:w-[15%] bg-red-500 hover:bg-red-600 p-2 rounded-xl text-white transition-all duration-300 flex items-center justify-center"
    >
      {loading ? <CircularProgress className="loading" /> : text}
    </button>
  ) : (
    <Link href={redirect}>
      <button
        className={`bg-[#783fca] hover:bg-[#a46ff0] ${
          funct === "edit" ? "w-[130px]" : "w-full"
        } p-2 rounded-xl text-white transition-all duration-300`}
      >
        {text}
      </button>
    </Link>
  );
};

export default Button;
