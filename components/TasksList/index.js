import { useEffect, useState } from "react";
import Button from "../interface/Button";
import axios from "axios";

const TasksList = () => {
  const [data, setData] = useState([]);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (typeof window !== "undefined") {
      setWidth(window.innerWidth);
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      await axios
        .get(`${process.env.API_BASE_URL}api/tasks`)
        .then((resp) => {
          setData(resp.data.data);
        })
        .catch((e) => console.log(e));
    } catch (error) {
      console.log(error);
    }
  };

  {
    return width < 1024 ? (
      data.map((e) => {
        return (
          <div className="my-4  rounded-xl p-4 h-full w-[95%] mx-auto border-2 border-gray-200">
            <div className="flex justify-between">
              <div className="w-full">
                <p className="font-semibold w-full">Nombre</p>
                <p className="w-full">{e.name}</p>
              </div>
              <div className="w-full">
                <p className="font-semibold w-full">Descripción</p>
                <p className="w-full box">{e.description}</p>
              </div>
            </div>
            <div className="mt-6 flex items-center justify-start gap-4">
              <Button funct={"edit"} task_id={e.id} />
              <Button funct={"delete"} task_id={e.id} />
            </div>
          </div>
        );
      })
    ) : (
      <table className="mt-6 w-full lg:ml-4">
        <thead className="h-[50px]">
          <tr className="text-left">
            <th>Tarea</th>
            <th>Descripción</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody className="h-[100px]">
          {data.map((e) => {
            return (
              <tr key={e.id}>
                <td>{e.name}</td>
                <td>{e.description}</td>
                <td className="w-full h-[100px] flex items-center gap-2">
                  <Button funct={"edit"} task_id={e.id} />
                  <Button funct={"delete"} task_id={e.id} />
                </td>
              </tr>
            );
          })}
        </tbody>
        <tbody></tbody>
      </table>
    );
  }
};

export default TasksList;
