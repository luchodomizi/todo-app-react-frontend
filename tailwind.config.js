/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  plugins: [
    "postcss-import",
    "postcss-flexbugs-fixes",
    "tailwindcss/nesting",
    "postcss-custom-properties",
    "tailwindcss",
    "autoprefixer",
  ],
};
